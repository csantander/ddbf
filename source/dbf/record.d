/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.3
 * Date: February 25, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.3 - February 25, 2013
 * 		$(UL
 * 			$(LI Updated for D2)
 * 			$(LI Added foreach capability)
 * 			$(LI Added toStringArray)
 * 			$(LI toString now uses square braces instead of curly braces so that writeln(r.toString) and writeln(r.toStringArray) print the same text)
 * 		)
 * 	)
 * 	$(LI Version 0.2n - May 23, 2006: Removed Mango dependency)
 * 	$(LI Version 0.2 - May 22, 2006: Record now implements IWritable. Added Record.toString)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 */
module dbf.record;

private:
import std.string;
import std.traits : Unqual;
import std.typetuple : staticIndexOf, TypeTuple;
import std.variant;

import dbf.boolean;
import dbf.datafile;
import dbf.date;

public:

/// A single record in the DBF
class Record
{
	/// Sets a field by its index
	void opIndexAssign (T) (T val, uint index)
		if (staticIndexOf!(Unqual!T, TypeTuple!(Boolean, string, long, int, double, Date, bool)) != -1)
	{
		static if (is(T : bool))
			elements [index] = (val ? Boolean.True : Boolean.False);
		else
			elements [index] = val;
	}
	
	/// Sets a field by its name
	void opIndexAssign (T) (T val, string fieldName)
		if (staticIndexOf!(Unqual!T, TypeTuple!(Boolean, string, long, int, double, Date, bool)) != -1)
	{
		static if (is(T : bool))
			elements [dbf.getFieldIndex (fieldName)] = (val ? Boolean.True : Boolean.False);
		else
			elements [dbf.getFieldIndex (fieldName)] = val;
	}
	
	/**
	 * Gets the value of a field
	 * Params:
	 * 	index = the _index of the field
	 * Returns: the value of the field as a std.boxer.Box
	 */
	Variant opIndex (uint index)
	{
		return elements [index];
	}
	
	/**
	 * Gets the value of a field
	 * Params:
	 * 	fieldName = the name of the field
	 * Returns: the value of the field as a std.boxer.Box
	 */
	Variant opIndex (string fieldName)
	{
		return elements [dbf.getFieldIndex (fieldName)];
	}
	
	/**
	 * Gets the value of a field
	 * Params:
	 * 	T = the type wanted to be returned
	 * 	index = the _index of the field
	 * Returns: the value of the field
	 */
	T get (T) (uint index)
	{
		return elements [index].get!(T)();
	}
	
	/**
	 * Gets the value of a field
	 * Params:
	 * 	T = the type wanted to be returned
	 * 	fieldName = the name of the field
	 * Returns: the value of the field
	 */
	T getByName (T) (string fieldName)
	{
		return elements [dbf.getFieldIndex (fieldName)].get!(T)();
	}
	
	/**
	 * Foreach the values in this record
	 */
	int opApply (int delegate (Variant) dg)
	{
		int res;

		foreach (Variant v; elements)
		{
			res = dg (v);
			if (res)
				break;
		}

		return res;
	}

	/// ditto
	int opApply (int delegate (uint, Variant) dg)
	{
		int res;

		foreach (uint i, Variant v; elements)
		{
			res = dg (i, v);
			if (res)
				break;
		}

		return res;
	}

	/// ditto
	int opApply (int delegate (string, Variant) dg)
	{
		int res;

		foreach (uint i, Variant v; elements)
		{
			res = dg (dbf.getFieldName(i), v);
			if (res)
				break;
		}

		return res;
	}

	/**
	 * Returns an array containing a string representation of each value in this record
	 */
	string[] toStringArray ()
	{
		string[] result;
		
		foreach (uint i, Variant elm; elements)
		{
			if (elm.type == typeid (Date))
				result ~= elm.get!(Date)().toString ();
			else
				result ~= strip (elm.toString ());
		}
		
		return result;
	}

	/**
	 * Returns a string representation
	 */
	override string toString () const
	{
		auto str = "[ ".dup;
		immutable n = elements.length;
		
		foreach (uint i, Variant elm; elements)
		{
			if (elm.type == typeid (Date))
				str ~= elm.get!(Date)().toString ();
			else
				str ~= strip (elm.toString ());

			if (i < n - 1)
				str ~= ", ";
		}
		
		return (str ~ " ]").idup;
	}

	package:
	DBF dbf;
	ubyte deletedFlag;
	Variant [] elements;

	this (DBF dbf)
	{
		this.dbf = dbf;
	}
}
