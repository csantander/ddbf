/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Module to load and save DBF files
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.6
 * Date: November 26, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.6 - November 26, 2013
 * 		$(UL
 * 			$(LI Replaced 'xformat' for 'format')
 * 			$(LI Added support for unsigned types in DBF.addRecord)
 * 		)
 * 	)
 * 	$(LI Version 0.5 - February 25, 2013
 * 		$(UL
 * 			$(LI Updated for D2)
 * 			$(LI If the DBF is constructed using a file name, the underlying stream is closed after loading/saving)
 * 			$(LI Added DBF.getRecordCount, DBF.getFieldCount, DBF.getFieldName, DBF.addDateField)
 * 		)
 * 	)
 * 	$(LI Version 0.4n - May 24, 2006
 * 		$(UL
 * 			$(LI Added support for non-integer numerical fields)
 * 			$(LI Added DBF.removeRecord and DBF.removeAllRecords)
 * 		)
 * 	)
 * 	$(LI Version 0.3n - May 23, 2006: Removed Mango dependency)
 * 	$(LI Version 0.3 - May 22, 2006
 * 		$(UL
 * 			$(LI Added DBF.addEmptyRecord)
 * 			$(LI Added DBF.this $(LPAREN)Buffer$(RPAREN) and proper support for it)
 * 			$(LI Removed alias DBF.read)
 * 			$(LI Versioned out DBF.this $(LPAREN)Stream$(RPAREN))
 * 			$(LI Better unittesting)
 * 		)
 * 	)
 * 	$(LI Version 0.2 - May 18, 2006
 * 		$(UL
 * 			$(LI Renamed public DBF.read to DBF.load (added a deprecated alias just in case))
 * 			$(LI Renamed private DBF.read to DBF.readText)
 * 			$(LI Added DBF.save, DBF.addField, DBF.addNumericField, DBF.removeField , DBF.getFieldIndex, DBF.opApply, DBF.addRecord)
 * 			$(LI Renamed FieldDescriptor to Field (added a deprecated alias just in case), moved to dbf.field)
 * 			$(LI Added Field.this and static Date.opCall)
 * 			$(LI Renamed DataTypes to DataType)
 * 			$(LI Changed type of Field.fieldType to DataType)
 * 			$(LI Added DBFException, moved to dbf.exception)
 * 			$(LI Moved getAndSwap, getAndSwap, swapAndPut, swapAndPut to dbf.internal)
 * 			$(LI Moved Boolean to dbf.boolean)
 * 			$(LI Moved Date to dbf.date)
 * 			$(LI Moved Record to dbf.record)
 * 			$(LI Moved DataType to dbf.datatype)
 * 		)
 * 	)
 * 	$(LI Version 0.1 - May 14, 2006: First public release)
 * )
 * License: Zlib
 * See_Also:
 * $(UL
 * $(LI $(LINK http://www.clicketyclick.dk/databases/xbase/format/dbf.html))
 * $(LI $(LINK http://www.dbase.com/Knowledgebase/INT/db7_file_fmt.htm))
 * )
 */

module dbf.datafile;

private:
import core.vararg;

import std.conv;
import std.datetime : Clock;
import std.conv;
import std.string;

import undead.stream;

import dbf.boolean;
import dbf.datatype;
import dbf.date;
import dbf.exception;
import dbf.field;
import dbf.internal;
import dbf.record;

debug import std.stdio : writefln, writeln, writef;

public:

/**
 * Simple class to access a _DBF file.
 * 
 * Loading and saving are performed from wherever the stream is at that moment,
 * and the stream is not closed after loading or saving. All this means that if
 * you have a Stream which contains more than just the _DBF, you can keep using
 * it.
 * Bugs:
 * $(UL
 * 	$(LI Only works for data types: L, C, N, D)
 * 	$(LI No character conversion is made, so non-Unicode strings will generate exceptions)
 * )
 * Examples:
 * --------------------
 * string file = args[1];
 * DBF dbf = new DBF (file);
 * dbf.read();
 * 
 * ...
 * 
 * void foo (Stream stream)
 * {
 * 	(new DBF(stream)).load();
 * }
 * --------------------
 * See_Also: $(LINK2 http://www.dsource.org/projects/mango,Mango)
 */
class DBF
{
	/**
	 * Initializes the DBF from a Stream
	 * Params:
	 * 	stream = the Stream containing the DBF or where the DBF should be saved
	 */
	this (Stream stream)
	{
		this.stream = stream;
	}
	
	/**
	 * Initializes the DBF from a _file
	 * Params:
	 * 	file = the _file containing the DBF or where the DBF should be saved
	 */
	this (string file)
	{
		filename = file;
		closeAtEnd = true;
	}
	
	/**
	 * Loads the DBF from the specified source
	 */
	void load ()
	{
		if (closeAtEnd)
		{
			debug writefln ("opening file '%s'", filename);
			stream = new File (filename);
		}

		readHeader ();
		fields.length = 0;
		readFieldDescriptorArray ();
		records.length = recordCount;
		records.length = 0;
		if (0 < recordCount)
			readRecords ();
		if (closeAtEnd) stream.close();
	}

	/**
	 * Saves the DBF to the specified source
	 */
	void save ()
	{
		lastUpdate = Date(Clock.currTime());
		
		versionNumber = 0x3;

		recordLength = 1;
		foreach (Field fd; fields)
			if (fd.fieldType == DataType.Number)
				recordLength += fd.fieldLength /*+ fd.decimalCount*/;
			else
				recordLength += fd.nonNumericalFieldLength;
		
		headerLength = to!ushort (32 // header length
			+ 1 // terminator (at the end, actually)
			+ 32 // length of each field definition
			* fields.length);
		
		recordCount = to!uint (records.length);

		debug writefln("recordLength=%s, headerLength=%s, recordCount=%s", recordLength, headerLength, recordCount);
			
		if (closeAtEnd)
		{
			debug writefln ("saving file '%s'", filename);
			stream = new File (filename, FileMode.Out);
		}

		writeHeader ();
		writeFieldDescriptorArray ();
		writeRecords ();

		if (closeAtEnd) stream.close();
	}

	/**
	 * Returns the number of records
	 */
	auto getRecordCount ()
	{
		return records.length;
	}

	/**
	 * Returns the number of fields
	 */
	auto getFieldCount ()
	{
		return fields.length;
	}

	/**
	 * Returns the name of a field by its index
	 */
	auto getFieldName (uint index)
	{
		return fields[index].name;
	}

	/**
	 * Returns the index of a field by its name
	 */
	auto getFieldIndex (string name)
	{
		return columnsMapping [name];
	}

	/**
	 * Removes a field from this DBF
	 * Throws: DBFException if there's no field named name
	 */
	void removeField (string name)
	{
		uint * pIndex = name in columnsMapping;
		if (pIndex is null)
			throw new DBFException ("no field named '"
				~ name ~ "' in this DBF");
		
		removeField (*pIndex);
	}
	
	/**
	 * Removes a field from this DBF
	 * Throws: DBFException if index is greater than the number of fields
	 */
	void removeField (uint index)
	{
		if (fields.length <= index)
		{
			throw new DBFException ( format (
				"index %s greater than number of fields (%s)",
				index, fields.length));
		}
		
		fields = fields [0 .. index] ~ fields [1 + index .. $];
		
		updateMappings ();
	}

	/**
	 * Adds a field to this DBF
	 * 
	 * If type == DataType.Number, flength represents the fieldLength, and the
	 * decimalCount is set to 0.
	 * 
	 * If type == DataType.Date, flength is ignored and automatically set to 8.
	 * 
	 * This method fails if there're already records in the DBF. To modify the
	 * fields in the DBF, you must first remove all its records.
	 * Params:
	 * 	fname = the name of the field
	 * 	dataType = the type of the field
	 * 	flength = the length of the field
	 * Throws: DBFException if there're records in the DBF or if there's already
	 * a field with this name
	 */
	void addField (string fname, DataType dataType, ushort flength)
	{
		if (0 < records.length)
			throw new DBFException (
				"can't modify fields after inserting records");
		
		if (fname in columnsMapping)
			throw new DBFException ("field '" ~ fname
				~ "' already exists");
		
		auto fd = new Field;

		with (fd)
		{
			fieldName = fname;
			fieldType = dataType;
			
			if (dataType == DataType.Number)
			{
				fieldLength = to!ubyte (flength);
				decimalCount = 0;
			}
			else
			{
				switch (dataType)
				{
					case DataType.Logical:
						nonNumericalFieldLength = 1;
						break;
					case DataType.Character:
						nonNumericalFieldLength = flength;
						break;
					case DataType.Date:
						nonNumericalFieldLength = 8;
						break;
/+
					case DataType.Integer:
						nonNumericalFieldLength = 8;
						break;
+/
					default:
						assert (0);
				}
			}
		}
		
		addFD (fd);
	}
	
	/**
	 * Adds a numeric field to this DBF
	 * 
	 * This method fails if there're already records in the DBF. To modify the
	 * fields in the DBF, you must first remove all its records.
	 * Params:
	 * 	fname = the name of the field
	 * 	flength = the length of the field
	 * 	fdecimals = the decimal count of the field
	 * Throws: DBFException if there're records in the DBF or if there's already
	 * a field with this name
	 */
	void addNumericField (string fname, ubyte flength, ubyte fdecimals)
	{
		if (0 < records.length)
			throw new DBFException (
				"can't modify fields after inserting records");
		
		if (fname in columnsMapping)
			throw new DBFException ("field '" ~ fname
				~ "' already exists");
		
		Field fd = new Field;

		with (fd)
		{
			fieldName = fname;
			fieldLength = flength;
			decimalCount = fdecimals;
			fieldType = DataType.Number;
		}
		
		addFD (fd);
	}

	/**
	 * Adds a date field to this DBF
	 * 
	 * This method fails if there're already records in the DBF. To modify the
	 * fields in the DBF, you must first remove all its records.
	 * Params:
	 * 	fname = the name of the field
	 * Throws: DBFException if there're records in the DBF or if there's already
	 * a field with this name
	 */
	void addDateField (string fname)
	{
		if (0 < records.length)
			throw new DBFException (
				"can't modify fields after inserting records");
		
		if (fname in columnsMapping)
			throw new DBFException ("field '" ~ fname
				~ "' already exists");
		
		auto fd = new Field;

		with (fd)
		{
			fieldName = fname;
			fieldType = DataType.Date;
			nonNumericalFieldLength = 8;
		}
		
		addFD (fd);
	}

/+
	// TODO more documentation is needed for this
	/**
	 * Adds an integer field to this DBF
	 * 
	 * This method fails if there're already records in the DBF. To modify the
	 * fields in the DBF, you must first remove all its records.
	 * Params:
	 * 	fname = the name of the field
	 * Throws: DBFException if there're records in the DBF or if there's already
	 * a field with this name
	 */
	void addIntegerField (string fname)
	{
		if (0 < records.length)
			throw new DBFException (
				"can't modify fields after inserting records");
		
		if (fname in columnsMapping)
			throw new DBFException ("field '" ~ fname
				~ "' already exists");
		
		auto fd = new Field;

		with (fd)
		{
			fieldName = fname;
			fieldType = DataType.Integer;
			nonNumericalFieldLength = 4;
		}
		
		addFD (fd);
	}
+/

	/**
	 * Foreach the fields of this DBF
	 */
	int opApply (int delegate (Field) dg)
	{
		int res;

		foreach (Field f; fields)
		{
			res = dg (f);
			if (res)
				break;
		}

		return res;
	}

	/// ditto
	int opApply (int delegate (uint, Field) dg)
	{
		int res;

		foreach (uint i, Field f; fields)
		{
			res = dg (i, f);
			if (res)
				break;
		}

		return res;
	}

	/// ditto
	int opApply (int delegate (string, Field) dg)
	{
		int res;

		foreach (Field f; fields)
		{
			res = dg (f.fieldName, f);
			if (res)
				break;
		}

		return res;
	}

	/**
	 * Foreach the records of this DBF
	 */
	int opApply (int delegate (Record) dg)
	{
		int res;

		foreach (Record f; records)
		{
			res = dg (f);
			if (res)
				break;
		}

		return res;
	}

	/// ditto
	int opApply (int delegate (uint, Record) dg)
	{
		int res;

		foreach (uint i, Record r; records)
		{
			res = dg (i, r);
			if (res)
				break;
		}

		return res;
	}

	/**
	 * Removes a record from this DBF
	 * Throws: DBFException if index is greater than the number of records
	 */
	void removeRecord (uint index)
	{
		if (records.length <= index)
		{
			throw new DBFException ( format (
				"index %s greater than number of records (%s)",
				index, records.length));
		}
		
		Record deleted = records [index];
		
		records = records [0 .. index] ~ records [1 + index .. $];
		
		deleted.destroy();
	}

	/**
	 * Removes all records from this DBF
	 */
	void removeAllRecords ()
	{
		for (uint i; i < records.length; ++i)
		{
			auto r = records [i];
			r.destroy();
		}
		records.length = 0;
	}

	/**
	 * Adds a record to this DBF
	 * 
	 * This record contains no data and the fields must be explicitly filled by
	 * the user. However, it already belongs to the DBF.
	 */
	Record addEmptyRecord ()
	{
		Record rec = makeRecord ();
		records ~= rec;
		debug writefln("%s records", records.length);
		return rec;
	}

	/**
	 * Adds a record to this DBF
	 * 
	 * The types passed here must be compatible with those defined by the
	 * fields.
	 */
	void addRecord (...)
	{
		if (_arguments.length != fields.length)
			throw new DBFException
				("number of arguments doesn't match number of fields");
		
		Record rec = makeRecord ();
		
		for (size_t i; i < _arguments.length; ++i)
		{
			const ti = _arguments [i];
			
			if (ti == typeid (long))
				rec [i] = va_arg!(long) (_argptr);
			else if (ti == typeid (int))
				rec [i] = cast (long) va_arg!(int) (_argptr);
			else if (ti == typeid (short))
				rec [i] = cast (long) va_arg!(short) (_argptr);
			else if (ti == typeid (ulong))
				rec [i] = cast (long) va_arg!(ulong) (_argptr);
			else if (ti == typeid (uint))
				rec [i] = cast (long) va_arg!(uint) (_argptr);
			else if (ti == typeid (ushort))
				rec [i] = cast (long) va_arg!(ushort) (_argptr);
			else if (ti == typeid (string))
				rec [i] = va_arg!(string) (_argptr);
			else if (ti == typeid (Date))
				rec [i] = va_arg!(Date) (_argptr);
			else if (ti == typeid (bool))
				rec [i] = va_arg!(bool) (_argptr);
			else if (ti == typeid (Boolean))
				rec [i] = va_arg!(Boolean) (_argptr);
			else if (ti == typeid (double))
				rec [i] = va_arg!(double) (_argptr);
			else if (ti == typeid (float))
				rec [i] = cast(double) va_arg!(float) (_argptr);
			else
				throw new DBFException ("unknown type "
					~ _arguments [i].toString());
		}
		
		records ~= rec;
		
		debug writefln("%s records", records.length);
	}

	private
	{
		Stream stream;  /// the source of the DBF
		bool closeAtEnd = false; /// should we close the stream once we're done with it
		string filename; /// file name to use

		/*
		version number:
		03 - DBF without memo (FoxBASE+/FoxPro/dBASE III PLUS/dBASE IV)
		83 - DBF with memo (FoxBASE+/dBASE III PLUS)
		8B - dBASE III with memo
		F5 - FoxPro with memo
		*/
		ubyte versionNumber;

		Date lastUpdate;
		uint recordCount;
		ushort headerLength; // from the beginning of the file to the first data record
		ushort recordLength; // the sum of the field sizes plus 1 because of the deletion flag
		ubyte incompleteTransaction, encryptionFlag, mdxFlag, langDriver;
		Field [] fields;
		uint [ string ] columnsMapping;
		Record [] records;
		
		void readHeader ()
		{
			stream.read (versionNumber);
			debug writefln ("version number = %s", versionNumber);
			
			with (stream)
			{
				read (lastUpdate.month);
				lastUpdate.year = 1900 + lastUpdate.month;
				read (lastUpdate.month);
				read (lastUpdate.day);
			}
			debug
				writefln ("date of last update = %4d-%02d-%02d",
					lastUpdate.year,
					lastUpdate.month,
					lastUpdate.day);
			
			getAndSwap (stream, recordCount);
			debug writefln ("number of records in data file = %s",
				recordCount);
			
			getAndSwap (stream, headerLength);
			debug writefln ("length of header structure = %s",
				headerLength);
			
			getAndSwap (stream, recordLength);
			debug writefln ("length of each record = %s",
				recordLength);
			
			ubyte tmp;
			
			// reserved
			stream.read (tmp);
			stream.read (tmp);
			
			stream.read (incompleteTransaction);
			//debug writefln ("incomplete transaction = %s", incompleteTransaction);
			
			stream.read (encryptionFlag);
			//debug writefln ("encryption flag = %s", encryptionFlag);
			
			// Free record thread (reserved for LAN only)
			// ???
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			
			// Reserved for multi-user dBASE
			// ???
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
			stream.read (tmp);
	
			stream.read (mdxFlag);
			//debug writefln ("MDX flag = %s", mdxFlag);
	
			stream.read (langDriver);
			debug writefln ("language driver = %s", langDriver);
	
			// reserved
			stream.read (tmp);
			stream.read (tmp);
			
			debug writefln ("---------");
		}
		
		void readFieldDescriptorArray ()
		{
			uint index, size;
			ubyte tmp;
			ubyte nextByte;
			stream.read (nextByte);
			
			while (nextByte != 0xD)
			{
				Field fd = new Field ();
				fd.fieldName.length = 11;
				fd.fieldName.length = 0;
				bool append = true;
		
				for (int i; i < 11; ++i)
				{
					if (nextByte == 0)
						append = false;
					if (append)
						fd.fieldName ~= nextByte;
					stream.read (nextByte);
				}

				columnsMapping [fd.fieldName] = index++;
				debug writefln ("field name = %s", fd.fieldName);
				
				switch (nextByte)
				{
					case 'L':
						fd.fieldType = DataType.Logical;
						break;
					case 'N':
						fd.fieldType = DataType.Number;
						break;
					case 'C':
						fd.fieldType = DataType.Character;
						break;
					case 'D':
						fd.fieldType = DataType.Date;
						break;
/+
					case 'I':
						fd.fieldType = DataType.Integer;
						break;
+/
					default:
						throw new DBFException ("unknown type "
							~ cast (char) nextByte);
				}
				
				debug writefln ("field type = %s", cast (char) nextByte);
				
				getAndSwap (stream, fd.fieldDataAddress);
				//debug writefln ("field data address = %s", fd.fieldDataAddress);
	
				if (fd.fieldType == DataType.Number)
				{
					stream.read (fd.fieldLength);
					debug writefln ("field length = %s", fd.fieldLength);
					
					stream.read (fd.decimalCount);
					debug writefln ("decimal count = %s", fd.decimalCount);

					size += fd.fieldLength /*+ fd.decimalCount*/;
				}
				else
				{
					getAndSwap (stream, fd.nonNumericalFieldLength);
					debug writefln ("field length = %s", fd.nonNumericalFieldLength);
					
					size += fd.nonNumericalFieldLength;
				}
	
				getAndSwap (stream, fd.fieldFlags);
				//debug writefln ("field flags = %s", fd.fieldFlags);
				
				stream.read (fd.workAreaId);
				//debug writefln ("work area id = %s", fd.workAreaId);
				
				// Reserved for multi-user dBASE
				stream.read (tmp);
				stream.read (tmp);
				
				stream.read (fd.setFieldsFlag);
				//debug writefln ("flag for SET FIELDS = %s", fd.setFieldsFlag);
		
				// reserved
				stream.read (tmp);
				stream.read (tmp);
				stream.read (tmp);
				stream.read (tmp);
				stream.read (tmp);
				stream.read (tmp);
				stream.read (tmp);
				
				stream.read (fd.indexFieldFlag);
				//debug writefln ("index field flag = %s", fd.indexFieldFlag);
	
				fields ~= fd;
	
				stream.read (nextByte);
			}
			
			assert (headerLength == 32 + 1 + 32 * fields.length,
                format("headerLength (%s) != 32 + 1 + 32 * fields.length (%s)", headerLength, fields.length));
			
			debug writefln ("all fields size = %s", size);
			assert (1 + size == recordLength);
			
			debug writefln ("---------");
		}
		
		void readRecords ()
		{
			ubyte nextByte;
			stream.read (nextByte);
	
			while (nextByte != 0x1A)
			{
				Record rec = makeRecord ();
				
				rec.deletedFlag = nextByte;
				debug writefln ("record deleted flag = %s", rec.deletedFlag);
				
				foreach (uint i, Field fd; fields)
				{
					debug writef ("%s : ", fd.fieldName);
					
					switch (fd.fieldType)
					{
						case DataType.Logical:
							Boolean value;
							char data;
							stream.read (data);
							switch (data)
							{
								case 'T', 't', 'y', 'Y':
									value = Boolean.True;
									break;
								case 'F', 'f', 'n', 'N':
									value = Boolean.False;
									break;
								case '?', ' ':
									value = Boolean.Undefined;
									break;
								default:
									value = Boolean.Undefined;
							}
							
							rec [i] = value;
							
							debug writeln (value);
							break;
							
						case DataType.Number:
							if (fd.decimalCount == 0)
							{
								const value = readLong (fd.fieldLength);
								rec [i] = value;
								
								debug writeln (value);
							}
							else
							{
								const value = readDouble (fd.fieldLength, fd.decimalCount);
								rec [i] = value;
								
								debug writeln (value);
							}
							
							break;
							
						case DataType.Character:
							string data = readText (fd.nonNumericalFieldLength);
							rec [i] = data;
							
							debug writeln (data);
							break;
							
						case DataType.Date:
							auto year = to!short (readInt (4));
							auto month = to!ubyte (readInt (2));
							auto day = to!ubyte (readInt (2));
 							rec [i] = Date (year, month, day);
							
							debug writefln ("%04d-%02d-%02d", year, month, day);
							break;
/+
						case DataType.Integer:
							int data = readInteger ();
							rec [i] = data;
							break;
+/
						default:
							throw new DBFException ("unkwnown type");
					}
				}
					
				records ~= rec;
				
				if (records.length < recordCount)
					if (stream.eof ())
						nextByte = 0x1A;
					else
						stream.read (nextByte);
				else
					nextByte = 0x1A;
			}
			
			debug writefln ("records read = %s", records.length);
			assert (records.length == recordCount);
		}

		string readText (int count)
		{
			char[] buf;
			buf.length = count;
			foreach (ref char c; buf)
				stream.read (c);
			return buf.idup;
		}

		int readInt (int count)
		{
			auto txt = strip (readText (count));
			if (txt == "")
				return 0;

			return to!(int) (txt);
		}
		
		long readLong (int count)
		{
			auto txt = strip (readText (count));
			if (txt == "")
				return 0;

			return to!(long) (txt);
		}
		
		double readDouble (int count, int decimals)
		{
			auto txt = strip (readText (count));
			if (txt == "")
				return 0;

			return to!(double) (txt);
		}
		
		int readInteger ()
		{
			int v;
			getAndSwap (stream, v);
			return v;
		}
		
		void writeHeader ()
		{
			stream.write (versionNumber);
			
			auto tmp = to!ubyte (lastUpdate.year - 1900);
			with (stream)
			{
				write (tmp);
				write (lastUpdate.month);
				write (lastUpdate.day);
			}
			
			swapAndPut (stream, recordCount);
			swapAndPut (stream, headerLength);
			swapAndPut (stream, recordLength);
			
			// TODO: support all these things
			tmp = 0;
			for (int i; i < 20; ++i)
				stream.write (tmp);
			
			stream.flush ();
		}
		
		void writeFieldDescriptorArray ()
		{
			ubyte tmp;
			
			foreach (Field fd; fields)
			{
				// according to Microsoft:
				// "FoxPro limits field names to ten characters"
				// so should the 11th byte be set to 0x0?
				for (int i; i < 11; ++i)
				{
					if (i < fd.fieldName.length)
						tmp = fd.fieldName [i];
					else
						tmp = 0;
					stream.write (tmp);
				}
				
				switch (fd.fieldType)
				{
					case DataType.Logical:
						tmp = 'L';
						break;
					case DataType.Number:
						tmp = 'N';
						break;
					case DataType.Character:
						tmp = 'C';
						break;
					case DataType.Date:
						tmp = 'D';
						break;
/+
					case DataType.Integer:
						tmp = 'I';
						break;
+/
					default:
						throw new DBFException (
							format ("unknown type %s", fd.fieldType));
				}
				
				stream.write (tmp);
				swapAndPut (stream, fd.fieldDataAddress);
	
				if (fd.fieldType == DataType.Number)
				{
					stream.write (fd.fieldLength);
					stream.write (fd.decimalCount);
				}
				else
					swapAndPut (stream, fd.nonNumericalFieldLength);
				
				// TODO: support all these things
				tmp = 0;
				for (int i; i < 14; ++i)
					stream.write (tmp);
			}
			
			tmp = 0xD;
			stream.write (tmp);
			stream.flush ();
		}
		
		void writeRecords ()
		{
			foreach (Record rec; records)
			{
				stream.write (rec.deletedFlag);
				
				foreach (uint i, Field fd; fields)
				{
					switch (fd.fieldType)
					{
						case DataType.Logical:
							const value = rec.get!(Boolean) (i);
							if (value is Boolean.True)
								stream.write ('T');
							else if (value is Boolean.False)
								stream.write ('F');
							else
								stream.write ('?');
							break;
							
						case DataType.Number:
							if (fd.decimalCount == 0)
								writeLong (fd.fieldLength,
									rec.get!(long) (i));
							else
								writeDouble (fd.fieldLength,
									fd.decimalCount,
									rec.get!(double) (i));
							break;
							
						case DataType.Character:
							writeText (fd.nonNumericalFieldLength,
									rec.get!(string) (i));
							break;
							
						case DataType.Date:
							Date d = rec.get!(Date) (i);
							writeInt (4, d.year);
							writeInt (2, d.month);
							writeInt (2, d.day);
							break;
							
/+
						case DataType.Integer:
							writeInteger (rec[i].coerce!int);
							break;
+/
						default:
							throw new DBFException("unkwnown type");
					}
				}
			}
			
			// apparently, 0x1A is not needed
			stream.flush ();
		}

		// fill with blanks at the end
		void writeText (int count, string value)
		{
			ulong toWrite;
			
			if (value.length < count)
				toWrite = count - value.length;
			
			for (uint i; i < count - toWrite; ++i)
				stream.write (value [i]);
			
			if (0 < toWrite)
				for (uint i; i < toWrite; ++i)
					stream.write (' ');
		}

		void writeInt (int count, int value)
		{
			writeText (count, to!(string)(value));
		}
		
		void writeLong (int count, long value)
		{
			writeText (count, to!(string)(value));
		}
		
		void writeDouble (int count, int decimals, double value)
		{
			writeText (count, format("%*.*f", count, decimals, value));
		}
		
		void writeInteger (int value)
		{
			swapAndPut (stream, value);
		}
		
		Record makeRecord ()
		{
			Record rec = new Record (this);
			rec.elements.length = fields.length;
			return rec;
		}

		void addFD (Field fd)
		{
			debug 
				with (fd) {
					if (type==DataType.Number)
						writefln ("adding field: name=%s, type=%s, numericLength=%s, decimals=%s", name, type, numericLength, decimals);
					else
						writefln ("adding field: name=%s, type=%s, length=%s", name, type, length);
				}
				
			fields ~= fd;
			columnsMapping [fd.fieldName] = to!uint (fields.length - 1);
		}

		void updateMappings ()
		{
			auto names = columnsMapping.keys.dup;
			if (0 < names.length)
				foreach (string n; names)
					columnsMapping.remove (n);
			
			foreach (uint i, Field fd; fields)
				columnsMapping [fd.fieldName] = i;
		}
	}
}

private:

version (UnitTest)
{
	import std.cstream;

	version (UnitTestSaveLoad)
	{
	}

	void main (string[] args)
	{
	version (UnitTestLoadOnly)
	{
		string file = args [1];
		DBF dbf = new DBF (file);
		dbf.load ();
	}
	else version (UnitTestSaveLoad)
	{
		void assertAll (DBF dbf)
		{
			foreach (string name, Field f; dbf)
				switch (name)
				{
					case "name":
						assert (f.type == DataType.Character);
						assert (f.length == 20);
						break;
					case "date":
						assert (f.type == DataType.Date);
						break;
					case "sub":
						assert (f.type == DataType.Logical);
						break;
					case "value":
						assert (f.type == DataType.Number);
						assert (f.numericLength == 4);
						assert (f.decimals == 0);
						break;
					case "value2":
						assert (f.type == DataType.Number);
						assert (f.numericLength == 6);
						assert (f.decimals == 2);
						break;
					default:
				}
			
			foreach (uint i, Record r; dbf)
			{
				switch (i)
				{
					case 0:
						assert (strip (*r [0].peek!string) == "carlos");
						assert (*r [1].peek!Date == Date (2006, 5, 18));
						assert (*r [2].peek!Boolean == Boolean.False);
						assert (*r [3].peek!long == 8);
						assert (*r [4].peek!double == 3.14);
						break;

					case 1:
						assert (strip (r.get!(string) (0)) == "hola");
						assert (r.get!(Date) (1) == Date (1982, 11, 30));
						assert (r.get!(Boolean) (2) == Boolean.Undefined);
						assert (r.get!(long) (3) == -2);
						assert (r.get!(double) (4) == 99.99);
						break;

					case 2:
					{
						assert (strip (r.getByName!(string) ("name")) == "test");
						assert (r.getByName!(Date) ("date") == Date (0, 0, 0));
						assert (r.getByName!(Boolean) ("sub") == Boolean.True);
						assert (r.getByName!(long) ("value") == 999);
						immutable v2 = r.getByName!(double) ("value2");
						assert (v2 == 1.009 // before saving
							|| v2 == 1.01); // after loading
						break;
					}
					case 3:
					{
						assert (strip (r.getByName!(string) ("name")) == "foo");
						assert (r.getByName!(Date) ("date") == Date (2006, 1, 1));
						assert (r.getByName!(Boolean) ("sub") == Boolean.True);
						assert (r.getByName!(long) ("value") == 2974);
						immutable v2 = r.getByName!(double) ("value2");
						assert (v2 == -432.1234 // before saving
							|| v2 == -432.1);   // after loading
						break;
					}
					default:
				}
			}
		}
		
		auto ms = new MemoryStream ();
		
		DBF dbf = new DBF (ms);
		dbf.addField ("name", DataType.Character, 20);
		dbf.addField ("foo", DataType.Number, 6);
		dbf.addField ("date", DataType.Date, 8);
		dbf.addField ("sub", DataType.Logical, 1);
		dbf.addField ("dummy", DataType.Logical, 1);
		dbf.addField ("value", DataType.Number, 4);
		dbf.addNumericField ("value2", 6, 2);

		assert ( dbf.getFieldIndex ("name") == 0);
		assert ( dbf.getFieldIndex ("foo") == 1);
		assert ( dbf.getFieldIndex ("date") == 2);
		assert ( dbf.getFieldIndex ("sub") == 3);
		assert ( dbf.getFieldIndex ("dummy") == 4);
		assert ( dbf.getFieldIndex ("value") == 5);
		assert ( dbf.getFieldIndex ("value2") == 6);
		
		dbf.removeField ("foo");
		dbf.removeField (3);
		
		assert ( dbf.getFieldIndex ("name") == 0);
		assert ( dbf.getFieldIndex ("date") == 1);
		assert ( dbf.getFieldIndex ("sub") == 2);
		assert ( dbf.getFieldIndex ("value") == 3);
		assert ( dbf.getFieldIndex ("value2") == 4);
		
		dbf.addRecord ("carlos", Date (2006, 5, 18), false, 8, 3.14);
		dbf.addRecord ("hola", Date (1982, 11, 30), Boolean.Undefined, -2, 99.99);
		dbf.addRecord ("test", Date (0, 0, 0), Boolean.True, 999, 1.009);
		
		auto newrec = dbf.addEmptyRecord ();
		newrec ["name"] = "foo";
		newrec ["date"] = Date (2006, 1, 1);
		newrec [2u] = true;
		newrec [3u] = 2974L;
		newrec [4u] = -432.1234;
		
		assertAll (dbf);
		
		dbf.save ();
		
		ms.position = 0;
		dbf = new DBF (ms);
		dbf.load ();
		
		assertAll (dbf);
		
		ms.close ();
	}
	}
}
