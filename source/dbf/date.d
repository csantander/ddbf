/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.4
 * Date: November 26, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.4 - November 26, 2013: Replaced 'xformat' for 'format')
 * 	$(LI Version 0.3 - February 5, 2013
 * 		$(UL
 * 			$(LI Updated for D2)
 * 			$(LI Added constructor that accepts a SysTime)
 * 		)
 * 	)
 * 	$(LI Version 0.2n - May 23, 2006: Removed Mango dependency)
 * 	$(LI Version 0.2 - May 22, 2006: Added Date.toString)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 */
module dbf.date;

private:
import std.datetime;
import std.string;

public:

/// Simple date representation
struct Date
{
	/**
	 * Struct ctor
	 * Params:
	 * 	year = the _year
	 * 	month = the _month
	 * 	day = the _day
	 * Returns: a new Date
	 */
	static Date opCall (short year, ubyte month, ubyte day)
	{
		Date d;
		d.year = year;
		d.month = month;
		d.day = day;
		return d;
	}
	
	/**
	 * Struct ctor
	 * Params:
	 * 	time = a std.date.SysTime value
	 * Returns: a new Date
	 */
	static Date opCall (SysTime time)
	{
		Date d;
		d.year = time.year;
		d.month = time.month;
		d.day = time.day;
		return d;
	}
	
	/// Date in ISO format
	string toString () const
	{
		return format ("%04d-%02d-%02d", year, month, day);
	}
	
	short year;   /// the parts of a Date
	ubyte month;  /// ditto
	ubyte day;    /// ditto
}
