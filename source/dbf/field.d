/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.3
 * Date: February 5, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.3 - February 5, 2013: Updated for D2)
 * 	$(LI Version 0.2 - May 22, 2006: Removed alias FieldDescriptor)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 */
module dbf.field;

private:
import dbf.datatype;

public:

/// Definition of each field (column) in a record
class Field
{
	public:
	/// field name
	@property string name () { return fieldName; }
	
	/// field type
	@property DataType type () { return fieldType; }
	
	/// field length, for non-numerical fields
	@property ushort length () { return nonNumericalFieldLength; }
	
	/// field length, for numerical fields
	@property ubyte numericLength () { return fieldLength; }

	/// number of decimals, for numerical fields
	@property ubyte decimals () { return decimalCount; }	
	
	package:
	string fieldName;
	DataType fieldType;
	uint fieldDataAddress;
	union
	{
		struct
		{
			ubyte fieldLength, decimalCount;
		}
		ushort nonNumericalFieldLength;
	}
	ushort fieldFlags; // Reserved for multi-user dBASE
	ubyte workAreaId, setFieldsFlag, indexFieldFlag;
}
