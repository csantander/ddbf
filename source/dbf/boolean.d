/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.2
 * Date: February 5, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.2 - February 5, 2013
 * 		$(UL
 * 			$(LI Updated for D2)
 * 			$(LI Boolean.toString now returns "T"/"F" instead of "True"/"False")
 * 		)
 * 	)
 * 	$(LI Version 0.1n - May 23, 2006: Removed Mango dependency)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 */
module dbf.boolean;

public:
/**
 * Simple class for boolean values
 * 
 * The sole purpose of this class is provide a means to define an undefined
 * boolean. I thought about using an enum, but for some reason this felt better.
 */
final class Boolean
{
	static Boolean True; /// the only allowed values
	static Boolean False; /// ditto
	static Boolean Undefined; /// ditto
	
	/**
	 * Returns a string representation
	 * 
	 * Undefined is represented as '?'
	 */
	override string toString () const
	{
		if (this is True)
			return "T";
		else if (this is False)
			return "F";
		else
			return "?";
	}

	private
	{
		// don't allow more instances to be created
		this () {}

		static this ()
		{
			True = new Boolean ();
			False = new Boolean ();
			Undefined = new Boolean ();
		}
	}
}
