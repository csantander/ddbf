/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software in
       a product, an acknowledgment in the product documentation would be
       appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/

/**
 * This imports _all DBF modules needed to work with the library
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.2
 * Date: February 5, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.2 - February 5, 2013: Added documentation links to other modules)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 * See_Also:  $(LINK2 boolean.html,dbf.boolean), $(LINK2 datafile.html,dbf.datafile), $(LINK2 datatype.html,dbf.datatype), $(LINK2 date.html,dbf.date), $(LINK2 exception.html,dbf.exception), $(LINK2 field.html,dbf.field), $(LINK2 record.html,dbf.record)
 */
module dbf;

public:
import dbf.boolean;
import dbf.datafile;
import dbf.datatype;
import dbf.date;
import dbf.exception;
import dbf.field;
import dbf.record;
