/*
Copyright (c) 2013, Carlos Santander Bernal

This software is provided 'as/is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.
*/

/**
 * Internal bits
 * Authors: Carlos Santander Bernal
 * Copyright: 2013 Carlos Santander Bernal
 * Version: 0.2
 * Date: February 6, 2013
 * History:
 * $(UL
 * 	$(LI Version 0.2 - February 6, 2013
 * 		$(UL
 * 			$(LI Updated for D2)
 * 			$(LI Added getAndSwap and swapAndPut versions that accept int)
 * 		)
 * 	)
 * 	$(LI Version 0.1n - May 23, 2006: Removed Mango dependency)
 * 	$(LI Version 0.1 - May 18, 2006: Initial release)
 * )
 * License: Zlib
 */
module dbf.internal;

private:
import undead.stream;

version (BigEndian)
	import std.bitmanip;

package:

// ugly hacks!!! hope they work...
version (none)
{
class TypeInfo_S3dbf8datafile4Date : TypeInfo {}
class TypeInfo_C3dbf8datafile7Boolean : TypeInfo {}
}

version (none)
void swap (ref ushort val)
{
	ubyte *p = cast (ubyte *) &val;
	immutable tmp = p [0];
	p [0] = p [1];
	p [1] = tmp;
}

/// Read a value and byte swap it in big endian machines
void getAndSwap (Stream stream, ref uint val)
{
	stream.read (val);
	version (BigEndian)
	{
		val = swapEndian(val);
	}
}

/// ditto
void getAndSwap (Stream stream, ref int val)
{
	stream.read (val);
	version (BigEndian)
	{
		val = swapEndian(val);
	}
}

/// ditto
void getAndSwap (Stream stream, ref ushort val)
{
	stream.read (val);
	version (BigEndian)
	{
		val = swapEndian(val);
	}
}

/// Write a value but byte-swapping it before in big endian machines
void swapAndPut (Stream stream, uint val)
{
	version (BigEndian)
	{
		val = swapEndian(val);
	}
	stream.write (val);
}

/// ditto
void swapAndPut (Stream stream, int val)
{
	version (BigEndian)
	{
		val = swapEndian(val);
	}
	stream.write (val);
}

/// ditto
void swapAndPut (Stream stream, ushort val)
{
	version (BigEndian)
	{
		val = swapEndian(val);
	}
	stream.write (val);
}
