DDBF
====

DBF Library for D

This is a very simple library to access DBF files from D. Consider it in beta state.

I have only tested it with DMD2 v2.061 on Windows and Mac OS X.

Comments, suggestions, ideas, criticism, are all well received, for both the source code and the docs. In particular, while I've tried to update the library to make use of the new D2 features, I don't think it's there all the way. Ranges, more templates, etc., are not fully included in the library, but I'd like to know how to properly include them.

I'm also including a simple DBF to CSV/XML converter. The Windows version includes a GUI--to use it, please use DFL from https://github.com/Rayerd/dfl.
